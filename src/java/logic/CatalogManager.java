/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logic;

import jakarta.annotation.PostConstruct;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author alexandre
 */
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

@ManagedBean
@ApplicationScoped

public class CatalogManager {
    private ArrayList<Product> products;
    
    public CatalogManager(ArrayList<Product> products){
        this.products=products;
    }
    public ArrayList<Product> getProducts(){
        return this.products;
    }
    public void setProducts(ArrayList<Product> products){
        this.products=products;
    }
    
    @PostConstruct
    public void initCatalog(){
        Product p1=new Product(1,"Pomme",1.5);
        Product p2 = new Product(2,"Poire",2);
        this.products.add(p1);
        this.products.add(p2);
    }
}

